import java.util.Arrays;
class InsertionSortRec{//NON VAAA
    public static void sort(int[] arr,int i){
        if(i + 1<arr.length && i>=0){
            if(arr[i+1]<arr[i]){
                swapSposta(arr,i+1);
                sort(arr,i);
            }
        }
    }
    public static void swapSposta(int[] arr,int n){
        int temp = arr[n];
        while(n>0 && arr[n]<arr[n-1]){
              arr[n]=arr[n-1];
              n--;
        }
        arr[n] = temp;
    }
    public static void main(String args[]){
        int[] array = {7,6,5};
        sort(array,0);
        System.out.println(Arrays.toString(array));
    }
}