class Primo{
    public static boolean primoRicorsivo(int n,int k){
        if(n%k==0){
            return false;
        }
        if(k*k>n){
            return true;
        }
        return primoRicorsivo(n,k+1);
    }
    public static void main(String args[]){
        System.out.println(primoRicorsivo(8,2));
    }
}