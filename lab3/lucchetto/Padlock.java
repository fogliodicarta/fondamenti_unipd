class Padlock{
    Padlock(){
        password="0000";
        aperto=true;
    }
    Padlock(String pw){
        password=pw;
        aperto=true;
    }
    public void open(String pw){
        aperto = pw.equals(password);
    }
    public void close(){
        aperto= false;
    }
    public void setPasswd(String oldpw,String newpw){
        password=oldpw.equals(password)?newpw:password;
    }
    public String toString(){
        return "lock" + (aperto?"aperto":"chiuso");
    }
    private String password;
    private boolean aperto;
}