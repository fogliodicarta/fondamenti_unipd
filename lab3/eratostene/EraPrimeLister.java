import java.util.*;
class EraPrimeLister{
    public static void listaPrimi(int n){
        int[] array = new int[n-1];
        for(int i = 0;i < array.length;i++){
            array[i]=i+2;
        }
        boolean[] isPrime = new boolean[array.length];
        Arrays.fill(isPrime,true);
        System.out.println(Arrays.toString(array));
        for(int i=0;i<array.length;i++){
            if(isPrime[i]){
                System.out.println(array[i]);
                for(int j=i+array[i];j<array.length;j+=array[i]){
                    isPrime[j]=false;
                }
            }
        }
    }
    public static void main(String args[]){
        Scanner in = new Scanner(System.in);
        System.out.println("Dimmi il numero fino a quale vuoi arrivare:");
        int numero = in.nextInt();
        listaPrimi(numero);
    }
}