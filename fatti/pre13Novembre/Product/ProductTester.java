import java.util.Scanner;

public class ProductTester
{   public static void main(String[] args)
    {
        // acquisizione dei dati dei due prodotti
        Scanner in = new Scanner(System.in);      
        System.out.println("Primo prodotto (nome e prezzo su 2 righe)");
        String name1 = in.nextLine();
        double price1 = Double.parseDouble(in.nextLine());
        System.out.println("Secondo prodotto (nome e prezzo su 2 righe)");
        String name2 = in.nextLine();
        double price2 = Double.parseDouble(in.nextLine());

        // creazione dei due prodotti 
        Product prodotto1= new Product(name1,price1);
        Product prodotto2= new Product(name2,price2);

        // ordinamento e stampa
        if (prodotto1.getPrice() < prodotto2.getPrice())
        {   Product temp = prodotto1;
            prodotto1 = prodotto2;
            prodotto2 = temp;
        } // ora prodotto1 e` sicuramente il prodotto piu` caro

        System.out.println("\nProdotti in ordine decrescente di prezzo:");
        System.out.println(prodotto1.getName() + ": " + prodotto1.getPrice()
                                + "\u20AC");
        System.out.println(prodotto2.getName() + ": " + prodotto2.getPrice()
                                + "\u20AC");

        // applichiamo uno sconto al prodotto1
        System.out.println("Inserire sconto (%) per prodotto piu` caro");
        double discount = Double.parseDouble(in.nextLine());
        prodotto1.reducePrice(discount);

        /* Attenzione: da qui in avanti duplichiamo codice gia` scritto in
           precedenza per ordinare gli oggetti e stamparli. Sarebbe piu`
           corretto scrivere un metodo statico ausiliario che svolge queste
           operazioni: in questo modo il codice verrebbe scritto una volta sola
           nel corpo del metodo ausiliario, e il metodo potrebbe venire 
           invocato due volte
        */

        // ordinamento e stampa
        if (prodotto1.getPrice() < prodotto2.getPrice())
        {   Product temp = prodotto1;
            prodotto1 = prodotto2;
            prodotto2 = temp;
        }

        System.out.println("\nProdotti in ordine decrescente di prezzo:");
        System.out.println(prodotto1.getName() + ": " + prodotto1.getPrice()
                                + "\u20AC");
        System.out.println(prodotto2.getName() + ": " + prodotto2.getPrice()
                                + "\u20AC");

        in.close(); // chiusura del flusso d'ingresso 
 
   }
}