import java.util.*;
class OrdinaStringhe{
    public static void main(String args[]){
        Scanner tast = new Scanner(System.in);
        String s1 ,s2 ,s3 = "";
        System.out.println("Dimmi tre stringhe");
        s1=tast.nextLine();
        s2=tast.nextLine();
        s3=tast.nextLine();
        String min,max,mid ="";
        if(s1.compareTo(s2)<0){
            //s1 < s2
            if(s1.compareTo(s3)<0){
                //s1<s2 e s1<s3
                min = s1;
                if(s3.compareTo(s2)>0){
                    //s1<s2<s3
                    mid = s2;
                    max = s3;
                }else{
                    //s1<s3<s2
                    mid = s3;
                    max = s2;
                }
            }else{
                //s1<s2 e s3<s1
                min=s3;
                mid=s1;
                max=s2;
            }
        }else{
            //s2<s1
            if(s2.compareTo(s3)<0){
                //s2<s1 e s2<s3
                min = s2;
                if(s3.compareTo(s1)<0){
                    //s2<s3<s1
                    mid=s3;
                    max=s1;
                }else{
                    //s2<s1<s3
                    mid=s1;
                    max=s3;
                }
            }else{
                //s3<s2<s1
                min=s3;
                mid=s2;
                max=s1;
            }
        }
        System.out.println("Le parole in ordine sono:\n"+min+"\n"+mid+"\n"+max);
    
    }
}