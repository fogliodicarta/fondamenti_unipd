class ComparaDouble{
    public static boolean isEqualDouble(double a, double b){
        final double EPSILON =1E-14;
        return Math.abs(a-b)<EPSILON;
    }
    public static void main(String args[]){
        double x=Math.sqrt(2);
        System.out.println(isEqualDouble(x*x,2));
    }
}