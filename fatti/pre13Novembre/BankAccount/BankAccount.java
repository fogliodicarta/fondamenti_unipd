class BankAccount{
    public BankAccount(){
        balance=0;
    }
    public BankAccount(double initialAmount){
        deposit(initialAmount);
    }
    public void deposit(double amount)
    {
       if(amount<0){
           System.out.println("errore deposito negativo");
       }else{
           balance+=amount;
       }
    }

    public void addInterest(double rate)
    {
        if(rate<0){
            System.out.println("errore tasso negativo");
        }else{
            balance+=balance*rate;
        }
    }

    public void withdraw(double amount)
    {
        if(amount<0){
            System.out.println("errore prelievo negativo");
        }else if(amount>balance){
            System.out.println("errore prelievo eccede il bilancio");
        }else{
            balance-=amount;
        }
    }

    public double getBalance()
    {  return balance;
    }

    private double balance;
}