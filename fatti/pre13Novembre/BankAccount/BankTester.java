class BankTester{
    public static void main(String args[]){
        BankAccount b = new BankAccount(600);
        System.out.println(b.getBalance());
        b.deposit(500);
        System.out.println(b.getBalance());
    }
}