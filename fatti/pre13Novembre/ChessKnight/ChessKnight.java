class ChessKnight{
    String casella;
    ChessKnight(String initialSquare){
        if(isChessValidSquare(initialSquare)){
            casella=initialSquare;
        }else{
            casella="a1";
        }
    }
    boolean isChessValidSquare(String chessSquare){
        char primo=chessSquare.charAt(0);
        char secondo=chessSquare.charAt(1);
        return primo<='h' && primo >='a' && secondo >='1' && secondo <='8';
    }
    boolean isKnightReachableSquare(String chessSquare){
        char xcav=casella.charAt(0);
        char ycav=casella.charAt(1);
        char xarr=chessSquare.charAt(0);
        char yarr=chessSquare.charAt(1);
        boolean out= isChessValidSquare(chessSquare)&&(Math.abs(xcav-xarr)==2 && Math.abs(ycav-yarr)==1) || (Math.abs(xcav-xarr)==1 && Math.abs(ycav-yarr)==2);
        return out;
    }
    void moveToSquare(String toChessSquare){
        if (isKnightReachableSquare(toChessSquare)){
            casella=toChessSquare;
        }
    }
    public String toString(){
        return "Cavallo in "+casella;
    }

}