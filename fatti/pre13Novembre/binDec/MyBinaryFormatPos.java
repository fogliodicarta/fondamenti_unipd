import java.util.*;
class MyBinaryFormatPos{
	static String complementa(String input){
		int ultimoUno = input.lastIndexOf('1');
		String out = input.substring(0,ultimoUno).replace("0","x").replace("1","0").replace("x","1");
		out = out + input.substring(ultimoUno, input.length());
		return out;
	}


	public static void main(String[] args){
		Scanner tastiera = new Scanner(System.in);
        System.out.println("Inserisci un numero");
		int n=tastiera.nextInt();
		 String binario="";
		for(int i=7;i>=0;i--){
			binario = Integer.toString(n%2)+ binario;
			n/=2;		
		}
		
		System.out.println(binario);
		System.out.println(complementa(binario));
	}
}
