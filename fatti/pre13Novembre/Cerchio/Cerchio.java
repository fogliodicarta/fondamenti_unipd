class Cerchio{
    /*
       Construttore della classe Cerchio: centro (x,y) e raggio
   */
   public Cerchio (double x, double y, double radius)
   {
    this.radius=radius;
    this.x=x;
    this.y=y;
   }

   /*
       Verifica se questo cerchio e il cerchio c sono coincidenti, 
       ovvero se hanno tutti i punti in comune
   */

   public boolean isCoincident(Cerchio c)
   {
	 return (c.x==this.x && c.y==this.y && this.radius==c.radius);
   }


   /*
       verifica se questo cerchio e' interno al cerchio c o se
       il cerchio c e' interno a questo cerchio
   */
   public boolean isEncircled(Cerchio c)
   {
	return distanza(this,c)< Math.abs(this.radius-c.radius);
   }

   /*
      verifica se questo cerchio e il cerchio c sono esterni, ovvero non
      sono contenuti uno nell'altro e non hanno alcun punto in comune
   */
   public boolean isExternal(Cerchio c)
   {
	 return distanza(this,c)> (this.radius+c.radius);
   }
          


   /*
      verifica se questo cerchio e il cerchio c sono secanti, 
      ovvero se hanno almeno un punto in comune.
   */
   public boolean isSecant(Cerchio c)
   {
    double d = distanza(this,c);
    return (this.radius-c.radius < d)&&(this.radius+c.radius < d) ;
   }

   /*
      Verifica se questo cerchio e il cerchio c sono tangenti,
      ovvero se hanno un solo punto in comune
   */
   public boolean isTangential(Cerchio c)
   {
    double d = distanza(this,c);
    //System.out.println(d+" "+(this.radius+c.radius));
    return !isCoincident(c) && ((this.radius-c.radius == d)||(this.radius+c.radius == d)) ;
   }

   private double distanza(Cerchio c1, Cerchio c2){
       return Math.sqrt((c2.x-c1.x)*(c2.x-c1.x)+(c2.y-c1.y)*(c2.y-c1.y));
   }
   
    double x,y,radius;
}