class NumeriPrimi{
    public static void main(String args[]){
        boolean divisibile = false;
        int n=6;
        for(int d=2;d<n && !divisibile;d++){
            divisibile = n%d==0;
        }
        if(divisibile){
            System.out.println("non primo");
        }else{
            System.out.println("primo");
        }
    }
}