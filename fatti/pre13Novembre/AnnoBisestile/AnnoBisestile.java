import java.util.*;
class AnnoBisestile{
    public static void main(String args[]){
        Scanner tast = new Scanner(System.in);
        System.out.println("Dimmi un anno");
        int anno = tast.nextInt();
        boolean bis;
        if(anno<1582){
            bis = (anno % 4) == 0;
        }else{
            bis = ((anno%400==0)||((anno%4==0) && (anno%100!=0)));
        }
        System.out.println("L'affermazione "+anno+" è bisestile è "+ bis);
    }
}