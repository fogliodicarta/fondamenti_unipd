import java.util.*;

class SottraiDate{
  final static int MINUTI_ORA=60;
  final static int MINUTI_GIORNO=1440;
  public static void main(String[] args){
    Scanner tast = new Scanner(System.in);
    System.out.println("Dimmi il primo orario");
    String primaOra = tast.nextLine();
    System.out.println("Dimmi il secondo orario");
    String secondaOra = tast.nextLine();
    int h1 = Integer.parseInt(primaOra.substring(0,2));
    int h2 = Integer.parseInt(secondaOra.substring(0,2));
    int m1 = Integer.parseInt(primaOra.substring(2,4));
    int m2 = Integer.parseInt(secondaOra.substring(2,4));
    
    System.out.println(String.format("%02d:%02d\n%02d:%02d",h1,m1,h2,m2));

    int orario1 = h1* MINUTI_ORA + m1; //convertito in minuti
    int orario2 = h2* MINUTI_ORA + m2;
    int diffOrario = (orario2 - orario1 + MINUTI_GIORNO)%MINUTI_GIORNO;
    System.out.println("Tempo trascorso: "+ diffOrario/MINUTI_ORA+" ore e "+diffOrario%MINUTI_ORA+ " minuti");
  }
}
