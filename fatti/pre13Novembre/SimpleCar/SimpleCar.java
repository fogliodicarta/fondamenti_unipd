class SimpleCar{

    SimpleCar(){
        speed=0;
        gear=0;
    }

    void brake(){
        if(speed==5){
            speed=0;
        }else{
            speed-=speed*SPEED_DECREMENT_PERCENT;
        }
    }

    void gearDown(){
        if(gear!=MIN_GEAR){
            gear--;
        }       
    }

    void gearUp(){
        if(gear!=MAX_GEAR){
            gear++;
        }
    }

    void speedUp(){
        if(speed!=0){
            speed+=speed*SPEED_INCREMENT_PERCENT;
        }else{
            speed= INITIAL_SPEED;
        }
    }

    @Override
    public String toString(){
        return String.format("SimpleCar: marcia = %d, velocita' = %f km/h",gear,speed);
    }

    private double speed;
    private int gear;

    final static double INITIAL_SPEED=10;
    final static int MAX_GEAR=6;
    final static int MIN_GEAR=0;
    final static double SPEED_DECREMENT_PERCENT=0.3;
    final static double SPEED_INCREMENT_PERCENT=0.5;
}