import java.util.*;
class Angoli{
  public static void main(String[] args){
    Scanner tast = new Scanner(System.in).useLocale(Locale.US);
    System.out.println("Dimmi l'angolo in gradi");
    double norm = tast.nextDouble()%360;
    System.out.println(norm);
    double rad = norm * Math.PI / 180;
    System.out.println(String.format("%f sin: %f cos: %f",rad,Math.sin(rad),Math.cos(rad)));
  }
}
