class SimpleTriangle {
    SimpleTriangle(int a,int b,int c){
        this.a=a;
        this.b=b;
        this.c=c;
    }

    public int perimeter(){
        return a+b+c;
    }

    public double area(){
        int p= perimeter()/2;
        return Math.sqrt(p*(p-a)*(p-b)*(p-c));
    }

    public String toString(){
        return a+" "+b+" "+c;
    }

    int a, b,c;
}