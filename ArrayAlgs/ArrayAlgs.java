class ArrayAlgs{
    public static int linearSearch(int[] arr, int size, int value){
        int indexVal=-1;
        boolean found=false;
        for(int i=0;!found && i<size;i++){
            if(arr[i]==value){
                indexVal=i;
                found=true;
            }
        }
        return indexVal;
    }
    public static int binarySearch(int[] arr,int start, int end, int value){
        if(start>end){
            return -1;
        }
        int indexMedio = (start+end)/2;
        int medio = arr[indexMedio];
        if(medio==value){
            return indexMedio;
        } else if(value > medio){
            return binarySearch(arr,medio+1,end,value);
        }else{
            return binarySearch(arr,start,medio-1,value);
        }  
    }
    public static void selectionSort(int[] arr,int size){
        for(int start=0;start<size;start++){
            int minPos=findMinPos(arr,start,size);
            swap(arr,start,minPos);
        }
    }
    public static int findMinPos(int[] arr,int start,int size){
        int min=arr[start];
        int minPos=start;
        for(int i=start;i<size;i++){
            if(arr[i]<min){
                min = arr[i];
                minPos=i;
            }
        }
        return minPos;
    }
    public static void swap(int[] arr,int i,int j){
        int temp = arr[i];
        arr[i]=arr[j];
        arr[j]=temp;
    }
    public static int[] mergeSort(int[] arr,int end){
        if(arr.length==1){
            return arr;
        }
        int[] arrleft = new int[end/2];
        int[] arrright = new int[end - end/2];
        System.arraycopy(arr,0,arrleft,0,end/2);
        System.arraycopy(arr,end/2,arrright,0,end-end/2);

        return merge(mergeSort(arrleft,end/2),mergeSort(arrright,end-end/2));
    }

    private static int[] merge(int[] arr1,int[] arr2){
        int i=0,j=0;
        int index=0;
        int[] unito = new int[arr1.length+arr2.length];
        while(i<arr1.length&&j<arr2.length){
            int n=arr1[i];
            int m=arr2[j];
            if(n<m){
                unito[index++]=arr1[i++];
            }else{
                unito[index++]=arr2[j++];
            }
        }
        while(j<arr2.length){
            unito[index++]=arr2[j++];
        }
        while(i<arr1.length){
            unito[index++]=arr1[i++];
        }
        return unito;
    } 
}