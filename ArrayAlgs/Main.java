import java.util.*;
class Main{
    public static void main(String args[]){
        //search
        int[] a={2,5,0,9,1,13};
        System.out.println("linearsearch = "+ArrayAlgs.linearSearch(a,6,9));
        int[] a2={1,2,3,4,5,6,7,8,9,10};
        System.out.println("binarysearch = "+ArrayAlgs.binarySearch(a2,0,10,9));

        //sorts
        ArrayAlgs.selectionSort(a,6);
        System.out.println("selectionsort = "+ Arrays.toString(a));
        int[] a3={2,5,0,9,1,13};
        int[] a4 = ArrayAlgs.mergeSort(a3,6);
        System.out.println("mergesort = "+ Arrays.toString(a4));
    }
}