import java.util.*;
class MyInefficientSorter{
    public static int[] resize(int[] array, int lunghezza){
        int[] risultato = new int[lunghezza];
        for(int i=0;i<array.length && i<lunghezza;i++){
            risultato[i]=array[i];
        }
        return risultato;
    }
    public static int findMinIndex(int[] array){
        int min = array[0];
        int minIndex=0;
        int i;
        for(i=1;i<array.length;i++){
            if (array[i]<min){ 
                min=array[i];
                minIndex=i;
            }
        }
        return minIndex;
    }
    public static void remove(int[] array,int index){
        for(int i=index;i<array.length-1;i++){
            array[i]=array[i+1];
        }
    }
    public static void main(String args[]){
        Scanner in = new Scanner(System.in);
        int[] inputArray = new int[20];
        int ncounter=0;
        while(in.hasNext()){
            if(ncounter>=inputArray.length){
                inputArray=resize(inputArray,inputArray.length+10);
            }
            inputArray[ncounter++]=in.nextInt();
        }
        inputArray=resize(inputArray,ncounter);
        int[] outputArray=new int[ncounter];
        int outputCounter = 0;
        while(inputArray.length>0){
            int minIndex = findMinIndex(inputArray);
            //System.out.println(Arrays.toString(inputArray)+"---"+Arrays.toString(outputArray));
            //System.out.println(outputCounter);
            //System.out.println(minIndex);
            outputArray[outputCounter++]=inputArray[minIndex];
            remove(inputArray,minIndex);
            inputArray=resize(inputArray,inputArray.length-1);
        }
        System.out.println(Arrays.toString(outputArray));
    }
}