import java.util.Scanner;
class StringRotator{
    public static void main(String args[]){
        Scanner in = new Scanner(System.in);
        System.out.println("Dimmi una stringa");
        String s = in.nextLine();
        System.out.println("Di quanto la ruoti?");
        int n = in.nextInt();
        n=n%s.length();
        if(n<0)
        {
            n=s.length() + n;
        }
        int primaLett = (s.length())-(n);
        String ruotata = s.substring(primaLett)+s.substring(0,primaLett);
        System.out.println(ruotata);
    }
}