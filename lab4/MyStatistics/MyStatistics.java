import java.util.*;
class MyStatistics{
    public static int[] resize(int[] array, int lunghezza){
        int[] risultato = new int[lunghezza];
        for(int i=0;i<array.length && i<lunghezza;i++){
            risultato[i]=array[i];
        }
        return risultato;
    }
    public static double media(int[] array){
        double sum=0;
        for(int i=0;i<array.length;i++){
            sum+=array[i];
        }
        return sum/array.length;
    }
    public static double deviazioneStd(int[] array){
        double sommatoria=0;
        double avg = media(array);
        for(int i =0;i<array.length;i++){
            sommatoria+=Math.pow((array[i]-avg),2);
        }
        return Math.sqrt(sommatoria/array.length);
    }
    public static void main(String args[]){
        Scanner in = new Scanner(System.in);
        int[] inputArray = new int[20];
        int ncounter=0;
        while(in.hasNext()){
            if(ncounter>=inputArray.length){
                inputArray=resize(inputArray,inputArray.length+10);
            }
            inputArray[ncounter++]=in.nextInt();
        }
        inputArray=resize(inputArray,ncounter);
        System.out.println(Arrays.toString(inputArray));
        double avg = media(inputArray);
        System.out.println(avg);
        System.out.println(deviazioneStd(inputArray));
    }

}