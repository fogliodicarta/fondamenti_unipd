class SimpleAirRoute{
    private double length;
    private double altitude;
    private String fromAirport;
    private String toAirport;
    SimpleAirRoute(String from, String to, double l, double h){
        fromAirport=from;
        toAirport=to;
        length=l;
        altitude=h;
    }
    double getAirRouteLength(){
        return length;
    }
    double getAirRouteMaxAltitude(){
        return altitude;
    }
    String getFromAirport(){
        return fromAirport;
    }
    String getToAirport(){
        return toAirport;
    }
    public String toString(){
        return "AirRoute "+fromAirport+"-"+toAirport+" distance "+ length+" altitude "+altitude+ "m" ;
    }
}