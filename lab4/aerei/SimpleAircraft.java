class SimpleAircraft{
    private String model;
    private String airport;
    private double maxLength;
    private double maxHeigth;
    private double maxKilometers;

    private double currentKm;
    private String state;

    SimpleAircraft(String m, String a, double maxL, double maxH, double maxKm){
        model = m;
        airport=a;
        maxLength=maxL;
        maxHeigth=maxH;
        maxKilometers=maxKm;
        currentKm=0;
        state = "LANDED";
    }
    double getMileage(){
        return currentKm;
    }
    String getState(){
        return state;
    }
    boolean goHangar(){
        if(!state.equals("FLYING")){
            state="MAINTENANCE";
            return true;
        }
        return false;
    }
    boolean land(){
        if(state.equals("FLYING")){
            state="LANDED";
            if(currentKm>maxKilometers){
                goHangar();
            }
            return true;
        }
        return false;
    }
    boolean setSchedule(SimpleAirRoute ar){
        if(state.equals("LANDED") && ar.getAirRouteMaxAltitude()<=maxHeigth && ar.getAirRouteLength()<=maxLength && currentKm<=maxKilometers && airport.equals(ar.getFromAirport())){
            state="SCHEDULED";
            return true;
        }
        return false;
    }
    boolean takeOff(){
        if(state.equals("SCHEDULED")){
            state="FLYING";
            return true;
        }
        return false;
    }
    public String toString(){
        return model + " " + state;
    }
}